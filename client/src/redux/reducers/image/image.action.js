import axios from "axios";

// Redux type
import { GET_IMAGE } from "./image.type";

import {API_URL} from "../../../../src/key";

export const getImage = (_id) => async (dispatch) => {
  try {
    const image = await axios({
      method: "GET",
      url: `${API_URL}/image/${_id}`,
    });

    return dispatch({ type: GET_IMAGE, payload: image.data });
  } catch (error) {
    return dispatch({ type: "ERROR", payload: error });
  }
};
